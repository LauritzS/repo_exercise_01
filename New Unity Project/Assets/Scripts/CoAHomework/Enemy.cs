﻿namespace Scripts.CoAHomework
{
    public class Enemy
    {

    }

    public class Joker : Enemy
    {
        public int jokerHealth = 100;
        public int jokerDamage = 33;

        public float jokerSpeed = 80.47f;
        public float jokerShield = 100f;

        public string jokerName = "Joker";
        public string jokerWeapon = "Knive";
        public string jokerWeakness = "Smoke";

        public bool jokerAlive = true;
        public bool jokerDead;
        public bool jokerRespawn = true;
        public bool jokerRegainHealth = true;
    }

    public class Penguin : Enemy
    {
        public int penguinHealth = 150;
        public int penguinDamage = 23;

        public float penguinSpeed = 50.66f;
        public float penguinShield = 200f;

        public string penguinName = "Penguin";
        public string penguinWeapon = "Shotgun";
        public string penguinWeakness = "Dexterity";

        public bool penguinAlive = true;
        public bool penguinDead;
        public bool penguinRespawn = false;
        public bool penguinRegainHealth = true;
    }

    public class TwoFace : Enemy
    {
        public int twoFaceHealth = 100;
        public int twoFaceDamage = 31;

        public float twoFaceSpeed = 75.12f;
        public float twoFaceShield = 150f;

        public string twoFaceName = "Two Face";
        public string twoFaceWeapon = "Pistol";
        public string twoFaceWeakness = "Coinflip";

        public bool twoFaceAlive = true;
        public bool twoFaceDead;
        public bool twoFaceRespawn = true;
        public bool twoFaceRegainHealth = false;
    }

    public class Riddler : Enemy
    {
        public int riddlerHealth = 100;
        public int riddlerDamage = 100;

        public float riddlerSpeed = 12.67f;
        public float riddlerShield = 300f;

        public string riddlerName = "The Riddler";
        public string riddlerWeapon = "Riddles";
        public string riddlerWeakness = "Meelee Attacks";

        public bool riddlerAlive = true;
        public bool riddlerDead;
        public bool riddlerRespawn = false;
        public bool riddlerRegainHealth = true;

    }

    public class Falcone : Enemy
    {
        public int falconeHealth = 100;
        public int falconeDamage = 100;

        public float falconeSpeed = 25.53f;
        public float falconeShield = 100f;

        public string falconeName = "Falcone";
        public string falconeWeapon = "Submachinegun";
        public string falconeWeakness = "Dexterity";

        public bool falconeAlive = true;
        public bool falconeDead;
        public bool falconeRespawn = false;
        public bool falconeRegainHealth = false;
    }
}


